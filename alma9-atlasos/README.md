AlmaLinux OS 9 for ATLAS
=============

This configuration can be used to set up a base image that ATLAS analysis
and offline releases can be installed on top of. So that each image would
not have to duplicate the same base components.

During the build you can, and should choose the GCC version to be installed,
explicitly.

You can build a GCC 13 based image with the following:

```bash
docker build -t atlas/alma9-atlasos:X.Y.Z -t atlas/alma9-atlasos:latest-gcc13 \
   --build-arg GCCVERSION=13.1.0 \
   --build-arg GCCPACKAGES="gcc_13.1.0_x86_64_el9 binutils_2.40_x86_64_el9" \
   --compress --squash .
```

Images
------

You can find pre-built images on
[atlas/alma9-atlasos](https://hub.docker.com/r/atlas/alma9-atlasos/).
