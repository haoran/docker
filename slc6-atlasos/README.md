SLC6 ATLAS OS
=============

This configuration can be used to set up a base image that ATLAS analysis
and offline releases can be installed on top of. So that each image would
not have to duplicate the same base components.

You can build it like:

```bash
docker build -t <username>/slc6-atlasos:latest --compress --squash .
```

Compiler
--------

Note that the image holds GCC 6.2.0 out of the box. This is because **all**
production mode ATLAS releases use this compiler at the moment. Once this
changes, we will probably have to do something more elaborate here.
(For instance introducing an additional layer that just installs the right
compiler. Into an image like `slc6-atlasos-gcc7`.)

Examples
--------

You can find some pre-built images on
[atlas/slc6-atlasos](https://hub.docker.com/r/atlas/slc6-atlasos/).
