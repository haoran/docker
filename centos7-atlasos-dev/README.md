CC7 ATLAS Software Development OS
=================================

This image adds libraries on top of `centos7-atlasos`, which are necessary for
a "full offline software development environment".

You can build a GCC 8 based image with the following:

```bash
docker build -t atlas/centos7-atlasos-dev:X.Y.Z-gcc8 -t atlas/centos7-atlasos-dev:latest-gcc8 \
   --build-arg BASEIMAGE=atlas/centos7-atlasos:latest-gcc8 \
   --compress --squash .
```

For building a GCC 11 based image, use something like:

```bash
docker build -t atlas/centos7-atlasos-dev:X.Y.Z-gcc11 -t atlas/centos7-atlasos-dev:latest-gcc11 \
   -t atlas/centos7-atlasos-dev:latest \
   --build-arg BASEIMAGE=atlas/centos7-atlasos:latest-gcc11 \
   --compress --squash .
```

Images
------

You can find pre-built images on
[atlas/centos7-atlasos-dev](https://hub.docker.com/r/atlas/centos7-atlasos-dev/).
